<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\CommentRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;


/**
 * @ApiResource(normalizationContext={"groups"={"comment"}})
 * @ApiFilter(SearchFilter::class, properties={"product": "exact"})
 * @ORM\Entity(repositoryClass=CommentRepository::class)
 */
class Comment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @Groups({"comment"})
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"comment"})
     * @ORM\Column(type="string", length=255)
     */
    private $content;

    /**
     * @Groups({"comment"})
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="comment")
     */
    private $product;

    /**
     * @Groups({"comment"})
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="comments")
     */
    private $iduser;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getIduser(): ?User
    {
        return $this->iduser;
    }

    public function setIduser(?User $iduser): self
    {
        $this->iduser = $iduser;

        return $this;
    }
}
